import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PhotoType } from '../model/photo.type.model';
import { environment } from '../../environments/environment';

@Injectable()
export class PhotoTypeService {

    //public _apiService: ApiService;
    constructor(public apiService: ApiService){
        //this._apiService = apiService;
     };

     getPhotoType() : Promise<ApiResult>{
         const url = `${environment.baseApiUrl}/PhotoType/GetAll`;
         return this.apiService.httpGet(url, false);
     }; 

     getPhotoTypeById(id) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/PhotoType/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    }; 

    createPhotoType(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/PhotoType/Create`;
        return this.apiService.httpPost(url, model, false);
    }; 

    updatePhotoType(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/PhotoType/Update`;
        return this.apiService.httpPut(url, model, false);
    }; 

     deletePhotoType(object) : Promise<ApiResult> {
         const url = `${environment.baseApiUrl}/PhotoType/Delete/${object.Id}`;
         return this.apiService.httpPut(url, object);
     };


     changePhotoType(object) : Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/PhotoType/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
}
