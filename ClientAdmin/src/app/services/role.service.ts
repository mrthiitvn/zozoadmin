import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiService} from '../services/api.service';
import {UserModel} from '../model/user.model';
import { ApiResult } from '../model/api.result.model';
@Injectable()
export class RoleService {
    constructor(private _apiService: ApiService) {

    }
    GetAll(): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/Role/GetAll`;
        return this._apiService.httpGet(url, true);
    }
    CreateRole(role: any): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Role/Create/${role.RoleName}`;
        return this._apiService.httpPost(url, null, true);
    }
    UpdateRole(role: any): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Role/Update?currRoleName=${role.CurrRoleName}&nameChanged=${role.RoleName}`;
        return this._apiService.httpPut(url, role, true);
    }
    DeleteRole(roleName: string): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/Role/Remove/${roleName}`;
        return this._apiService.httpDelete(url, true);
    }
}