import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Config } from '../model/config.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ConfigService {
    constructor(public apiService: ApiService){};

    getConfig(): Promise<ApiResult> {
        const url = `${ environment.baseApiUrl }/Config/GetAll`;
        return this.apiService.httpGet(url, false);
    };

    getConfigById(id): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Config/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    };

    getConfigType() : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Config/GetConfigType`;
        return this.apiService.httpGet(url, false);
    }; 

    createConfig(model): Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Config/Create`;
        return this.apiService.httpPost(url, model, false);
    };

    updateConfig(model): Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Config/Update`;
        return this.apiService.httpPut(url, model, false);
    };

    deleteConfig(object): Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Config/Delete/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };

    changeConfig(object): Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Config/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
    
}