import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { ConversationType } from '../model/conversation.type.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ConversationTypeService {

    //public _apiService: ApiService;
    constructor(public apiService: ApiService){
        //this._apiService = apiService;
     };

     getConversationType() : Promise<ApiResult>{
         const url = `${environment.baseApiUrl}/ConversationType/GetAll`;
         return this.apiService.httpGet(url, false);
     }; 

     getConversationTypeById(id) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/ConversationType/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    }; 

    createConversationType(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/ConversationType/Create`;
        return this.apiService.httpPost(url, model, false);
    }; 

    updateConversationType(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/ConversationType/Update`;
        return this.apiService.httpPut(url, model, false);
    }; 

     deleteConversationType(object) : Promise<ApiResult> {
         const url = `${environment.baseApiUrl}/ConversationType/Delete/${object.Id}`;
         return this.apiService.httpPut(url, object);
     };


     changeConversationType(object) : Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/ConversationType/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
}
