import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { ApiService} from '../services/api.service';
import {UserModel} from '../model/user.model';
import { ApiResult } from '../model/api.result.model';
@Injectable()
export class UserService {
    constructor(private _apiService: ApiService) {

    }
    GetAll(currentPage: number, valueSortType: string, searchModel: any, pageSize): Promise<ApiResult> {
        var search = null;
        if (searchModel != null) {
            search = JSON.stringify(searchModel);
        }
        let url = `${environment.baseApiUrl}/User/GetAll`;
        // ?page=${currentPage}&page_size=${pageSize}&sort=${valueSortType}&search=${search}`;
        return this._apiService.httpGet(url, true);
    }

    GetUser(id: string): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/User/GetById?id=${id}`;
        return this._apiService.httpGet(url, true);
    }

    CreateUser(user: UserModel): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/User/Create`;
        return this._apiService.httpPost(url, user, false);
    }

    UpdateUser(user: UserModel): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/User/Update`;
        return this._apiService.httpPut(url, user, true);
    }

    RemoveUser(user: UserModel): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/User/Remove`;
        return this._apiService.httpRemove(url, user, true);
    }
}