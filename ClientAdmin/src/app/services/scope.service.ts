import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Scope } from '../model/scope.model';
import { environment } from '../../environments/environment';

@Injectable()
export class ScopeService {

    //public _apiService: ApiService;
    constructor(public apiService: ApiService){
        //this._apiService = apiService;
     };

     getScope() : Promise<ApiResult>{
         const url = `${environment.baseApiUrl}/Scope/GetAll`;
         return this.apiService.httpGet(url, false);
     }; 

     getScopeById(id) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Scope/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    }; 

    createScope(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Scope/Create`;
        return this.apiService.httpPost(url, model, false);
    }; 

    updateScope(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Scope/Update`;
        return this.apiService.httpPut(url, model, false);
    }; 

     deleteScope(object) : Promise<ApiResult> {
         const url = `${environment.baseApiUrl}/Scope/Delete/${object.Id}`;
         return this.apiService.httpPut(url, object);
     };


     changeScope(object) : Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Scope/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
}
