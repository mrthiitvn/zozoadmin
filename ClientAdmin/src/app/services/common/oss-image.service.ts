import { ApiResult } from '../../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
@Injectable()
export class OssImageService {
    constructor(private _apiService: ApiService) {
    }
    getkey(): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/User/GetAll`;
        return this._apiService.httpGet(url, true);
    }

    singleUploadToOss(file, path, url, key, secret, callback) {
        var promise = new Promise((resolve, reject) => {
            var progress;
            let formData = new FormData();
            formData.append(`key`, path);
            formData.append(`file`, file, file.name);
            let xhr: XMLHttpRequest = new XMLHttpRequest();
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(<any>JSON.parse(xhr.response));
                    }
                    else if (xhr.status === 204) {
                        resolve({
                            success: true,
                        })
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };

            xhr.upload.onprogress = (event) => {
                //callback to show progress
                callback(Math.round(event.loaded / event.total * 100));
            };

            xhr.onprogress
            xhr.open('POST', url, true);
            xhr.setRequestHeader('access_key_id', key);
            xhr.setRequestHeader('secret_access_key', secret);
            xhr.send(formData);
        });
        return promise;
    }
}