import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SessionData } from '../model/session.data.model';
@Injectable()
export class AuthenticateService {
    private sessionObject = new BehaviorSubject<SessionData>(null);
    public returnUrl: string;
    constructor() {
        var Token = localStorage.getItem('Token');
        if (Token != null) {
            var Id = +localStorage.getItem('Id'); //put + to convert string to number
            var FirstName = localStorage.getItem('FirstName');
            var LastName = localStorage.getItem('LastName');
            var Email = localStorage.getItem('Email');
            var Admin = this.convertStringToBoolean(localStorage.getItem('Admin'));
            var AdminSupper = this.convertStringToBoolean(localStorage.getItem('AdminSupper'));
            var PictureUrl = localStorage.getItem('PictureUrl');
            var sessionData = new SessionData(Id, Email, FirstName, LastName, Token, PictureUrl, Admin, AdminSupper);
            this.setSessionData(sessionData);
        }
    }
    setSessionData(val: SessionData): void {
        localStorage.setItem('Id', val.Id!=null?val.Id.toString():null);
        localStorage.setItem('Email', val.Email!=null?val.Email:null);
        localStorage.setItem('Token', val.Token!=null?val.Token:null);
        localStorage.setItem('FirstName', val.FirstName!=null?val.FirstName:null);
        localStorage.setItem('LastName', val.LastName!=null?val.LastName:null);
        localStorage.setItem('PictureUrl', val.PictureUrl!=null?val.PictureUrl:null);
        localStorage.setItem('Admin', val.Admin!=null?val.Admin.toString():null);
        localStorage.setItem('AdminSupper', val.AdminSupper!=null?val.AdminSupper.toString():null);
        // localStorage.setItem('sessionData', JSON.stringify(val));
        this.sessionObject.next(val);
    }
    getSessionData() {
        return this.sessionObject;
    }
    clearSessionData() {
        localStorage.clear();
        this.sessionObject.next(null);
    }

    convertStringToBoolean(val: string): boolean {
        if (val == "true" || val == "yes")
            return true;
        else
            return false;
    }
    updateAvatar(avatar: string) {
        var session = this.getSessionData().value;
        session.PictureUrl = avatar;
        this.setSessionData(session);
    }
    buildTokenSessionData(token: any): SessionData {
        var sessionData = new SessionData(null, null, null, null, token, null, null,  null);
        
        return sessionData;
    }

    buildSessionData(authResult: any): SessionData {
        var user = authResult;
        var Admin = false;
        var AdminSupper = false;
        var Roles = user.Roles;
        if(Roles.length > 0){
            for(var i = 1; i <= Roles.length; i++){
                if (Roles[i] == "Admin") {
                    Admin = true;
                }
                if (Roles[i] == "AdminSupper") {
                    AdminSupper = true;
                }
            }
        }
        var sessionData = new SessionData(user.Id, user.Email, user.FirstName, user.LastName,
            user.Token, user.PictureUrl, Admin,  AdminSupper);
        return sessionData;
    }

    updateSessionData(key: string, value: any) {
        var session = this.getSessionData().value;
        session[key] = value;
        this.setSessionData(session);
    }
}