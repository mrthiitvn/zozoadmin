export class UserModel {
    constructor() { }
    public Id: number;
    public FirstName: String;
    public LastName: String;
    public Email: string;
    public EmailConfirm: String;
    public Password: String;
    public PasswordConfirm: String;
    public phoneNumber: String;
    public Recaptcha: String;
    public PictureUrl: any;
    public Location: string;
    public FacebookId: number;
    public RoleId: number;
    public Status: number;
    public createdAt: Date;
    public SelectedRoleId:number;
}