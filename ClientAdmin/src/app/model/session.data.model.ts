export class SessionData {
    public Id: number;
    public Email: string;
    public Token: string;
    public FirstName: string;
    public LastName: string;
    public Admin: boolean;
    public AdminSupper: boolean;
    public PictureUrl: string;
    constructor(Id: number, Email: string, FirstName: string, LastName: string, Token: string, PictureUrl: string, Admin: boolean, AdminSupper: boolean) {
        this.Id = Id;
        this.Email = Email;
        this.Token = Token;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Admin = Admin;
        this.PictureUrl = PictureUrl;
        this.AdminSupper = AdminSupper;
    }

    updateToken(newToken: string) {
        this.Token = newToken;
    }
}
