export class Config {
    public id: number;
    public name: string;
    public value: string;
    public configTypeId: number;
    public status: number;
}