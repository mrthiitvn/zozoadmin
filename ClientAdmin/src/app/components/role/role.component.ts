import { Component, OnInit, ViewChild } from '@angular/core';
import { RoleService } from '../../services/role.service';
import { ModalDirective } from 'ngx-bootstrap';
import swal from 'sweetalert2';
import { Title } from '@angular/platform-browser';
import { Utils } from '../../utils/utils';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  public roleList: any;
  public totalItem: number;
  public selectedRole: any = {
    Id: null,
    RoleName: '',
    Status: 1
  };
  public currRoleName : string = '';
  @ViewChild('RoleModal') public RoleModal: ModalDirective;
  @ViewChild('DeleteRoleModal') public DeleteRoleModal: ModalDirective;
  constructor(private roleService: RoleService,
    private titleService: Title,
    private utils: Utils
    ) {
    this.titleService.setTitle("Role");
   }

  ngOnInit() {
    this.GetAll();
  }

  GetAll(): void {
    this.roleService.GetAll().then(result => {debugger;
      if(result != null && result.data != null){
        this.roleList = result.data;
        this.totalItem = result.data.length;
      }
      else{
        swal('','get all user error', 'error');
      }
    }).catch(function(error){
      swal('',error, 'error');
    });
  }

  OpenRoleModal(role): void {
    if(role != null){
      this.selectedRole = this.utils.clone(role);
      this.currRoleName = this.utils.clone(role.RoleName);
    }
    else{
    this.selectedRole.Id = '';
    }
    this.RoleModal.show();
  }

  Save(): void {
    if(this.selectedRole.Id != ''){
      this.selectedRole.CurrRoleName = this.currRoleName;
      this.roleService.UpdateRole(this.selectedRole).then(result => {
        if(result != null && result.success){
          this.GetAll();
          swal('','Cập nhật role thành công', 'success');
          this.RoleModal.hide();
        }
        else{
          swal('', 'Cập nhật role thất bại', 'error');
        }
      }).catch(function(error){
        swal('', 'Cập nhật role thất bại', 'error');
      });
    }
    else{
      this.roleService.CreateRole(this.selectedRole).then(result => {
        if(result != null && result.success){
          this.GetAll();
          swal('','Tạo role thành công', 'success');
          this.RoleModal.hide();
        }
        else{
          swal('', 'Tạo nhật role thất bại', 'error');
        }
      }).catch(function(error){
        swal('', 'Tạo nhật role thất bại', 'error');
      });
    }
  }

  OpenDeleteModal(): void {
    this.DeleteRoleModal.show();
  }

  Delete(): void {
    this.roleService.DeleteRole(this.selectedRole.RoleName).then(result => {
      if(result != null && result.success){
        this.GetAll();
        swal('', 'Xóa role thành công.', 'success');
        this.DeleteRoleModal.hide();
      }
      else{
        swal('', 'Xóa role thất bại.', 'error');
      }
    }).catch(function(error){
      swal('', 'Xóa role thất bại.', 'error');
    });
  }
}
