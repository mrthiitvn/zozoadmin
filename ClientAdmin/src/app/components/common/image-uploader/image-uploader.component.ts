import { OssImageService } from '../../../services/common/oss-image.service';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { ChangeDetectorRef, Component, EventEmitter, Input, NgZone, OnInit, Output, ViewChild } from '@angular/core';

declare var $: any;
declare var swal: any;

@Component({
    selector: 'image-uploader',
    templateUrl: 'image-uploader.component.html'
})

export class ImageUploaderComponent implements OnInit {
    @Input() numberOfFiles: number;
    @Input() initFiles: string[] = [];
    @Output() onSaveFiles = new EventEmitter<string[]>();
    @Output() onDeleteFile = new EventEmitter<string[]>();
    @Output() onFilesChangedEvent = new EventEmitter<string[]>();
    @Output() onCancel = new EventEmitter<boolean>();
    @Input() fileSizeLimit: number = 5242880; //(5mb )
    @Input() destDir: string;
    @Input() allowedExtensions: string;
    @ViewChild('image_upload_picker') public image_upload_picker: HTMLInputElement;
    public files: Array<any>
    public conponent_changed: boolean = false;
    public error_message: string = null;
    public success_message: string = null;
    public allowUpload: boolean = true;
    public limitTimeCheckForInitFile = 5;
    public allow_add:boolean = false;
    constructor(private ossService: OssImageService,
        private ref: ChangeDetectorRef,
        private zone: NgZone) { }
    ngOnInit(): void {
        this.init();
    }

    ngAfterViewInit() {


    }
    
    public clearFiles(): void {
        if (this.files != undefined && this.files != null) {
            this.files.splice(0, this.files.length);
        }
    }

    init() {
        var self = this;
        this.files = new Array<String>();
        setTimeout(function () {
            //console.log('setTimeout');
            if (self.initFiles != undefined && self.initFiles != null) {
                self.files.push({
                    display_url: self.initFiles,
                    file: null
                });
            }
        }, 3000);
    }

    validateFileExtension(word): boolean {
        return this.allowedExtensions.indexOf(word.toLowerCase()) > -1;
    }

    save() {
        if (this.conponent_changed == false || this.allowUpload == false || this.error_message != null) {
            return;
        }
        var self = this;
        this.ossService.getkey().then(result => {
            if (result.success) {
                var data = result.data;
                var promiseAll = [];
                this.files.forEach(element => {
                    console.log(element);
                    //upload new file and save
                    if (element.file != undefined && element.file != null) {
                        var fileName = self.destDir + "/" + self.generateFileName(element.file);
                        var promise = this.ossService.singleUploadToOss(element.file, fileName, data.url, data.key, data.secret, function (progress) {
                            self.zone.run(() => {
                                element.progress = progress;
                                element.file = null;
                                self.ref.detectChanges();
                            });
                        }).then(result => {
                            element.display_url = data.url + "/" + fileName;
                            element.progress = 100;
                        }).catch(err => {
                            console.log(err);
                        });
                        promiseAll.push(promise);
                    }
                });
                Promise.all(promiseAll).then(result => {
                    var res = [];
                    //console.log(this.files);
                    this.files.forEach(element => {
                        res.push(element.display_url);
                    });
                    //console.log(res);
                    this.onSaveFiles.emit(res);
                    this.allowUpload = false;
                    this.onFilesChange();
                });
            }
        })
    }

    removeFile(item, index) {
        this.conponent_changed = true;
        this.error_message = null;
        this.allowUpload = false;

        this.files.splice(index, 1);
        for (var i = 0; i < this.files.length; i++) {
            if (this.files[i].file != undefined || this.files[i].file != null) {
                this.allowUpload = true;
                break;
            }
        }
        this.onDeleteFile.emit(item);
        this.onFilesChange();
        console.log(this.files);
    }

    cancel() {
        this.onCancel.emit();
    }

    select(event) {
        var self = this;
        this.conponent_changed = true;
        this.error_message = null;
        var files: File[] = event.target.files;

        //validate number of files
        var allow_add_file = true;
        var new_file = files[0];
        var array_name = new_file.name.split(".", new_file.name.length);
        var file_name = array_name[0].split(' ').join('-');
        
        for(var i = 0; i < self.files.length; i++){
            if(self.files[i].display_url.indexOf(file_name) != -1){
                allow_add_file = false;
                break;
            }
        }
        if (files.length + this.files.length > this.numberOfFiles && allow_add_file) {
            this.fireMaxImageError();
            $('#image_upload_picker').val('');
            return;
        }
        //validate file size
        // for (var i = 0; i < files.length; i++) {
        //     if (files[i].size > this.fileSizeLimit) {
        //         this.fireMaxSizeError();
        //         $('#image_upload_picker').val('');
        //         break;
        //     }
        // }
        for (var i = 0; i < files.length; i++) {
            this.readfile(files[i]);
        }
        $('#image_upload_picker').val('');
        this.save();
    }

    readfile(file) {
        var self = this;
        console.log(file);
        var reader: FileReader = new FileReader();  
        reader.addEventListener("loadend", function () {
            var allow_add_file = true
            var array_name = file.name.split(".", file.name.length);
            var file_name = array_name[0].split(' ').join('-');
            for(var i = 0; i < self.files.length; i++){
                if(self.files[i].display_url.indexOf(file_name) != -1){
                    allow_add_file = false;
                    break;
                }
            }
            if(allow_add_file){
                self.files.push({
                    display_url: this.result,
                    file: file,
                    progress: 0
                });
            }  
        }, false);
        var fileExtension = file.name.split('.').pop().toLowerCase();
        if(this.validateFileExtension(fileExtension)) {
            if (file) {
                reader.readAsDataURL(file);
            } 
        }
        else{
            swal('','upload error', 'error');
        }
        // else {
        //     this.files.push({
        //         display_url: file.name,
        //         file: file,
        //         progress: 0
        //     });
        // }
        this.allowUpload = true;
    }

    fireMaxImageError() {
        this.error_message = "Maximum number of files allow: " + this.numberOfFiles;
    }

    fireMaxSizeError() {
        this.error_message = "Maximum file size allow: " + this.fileSizeLimit / (1024 * 1024) + " Mb.";
    }

    generateFileName(file: File) {
        var date = new Date();
        var sufix = date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
        var fileName = file.name.split('.')[0];
        var fileExtension = file.name.split('.').pop();
        fileName = fileName.replace(/[ ()$%^!@#&+~]+/g,'-');
        return fileName + '_' + sufix + '.' + fileExtension;
    }

    blockInput() {

    }

    unblockInput() {

    }

    onFilesChange(): void {
        var files_path = this.files.map(file => file.display_url);
        this.onFilesChangedEvent.emit(files_path);
    }
}