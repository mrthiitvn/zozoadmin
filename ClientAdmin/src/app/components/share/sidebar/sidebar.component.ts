import { Component, OnInit } from '@angular/core';
import { SessionData } from '../../../model/session.data.model';
import { AuthenticateService } from '../../../services/authenticate.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public session: SessionData;
  constructor(
    private authenticateService: AuthenticateService
  ) { 
    this.authenticateService.getSessionData().subscribe(res => {
      this.session = res;
    });
  }

  ngOnInit() {
  }

}
