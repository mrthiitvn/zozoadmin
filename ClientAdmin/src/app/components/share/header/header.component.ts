import { Component, OnInit } from '@angular/core';
import { SessionData } from '../../../model/session.data.model';
import { AuthenticateService } from '../../../services/authenticate.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public session: SessionData;
  constructor(
    private authenticateService: AuthenticateService
  ) {
    this.authenticateService.getSessionData().subscribe(res => {
      this.session = res;
    });
   }

  ngOnInit() {
  }

}
