import { Component, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { ConversationTypeService } from '../../services/conversation.type.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
//Import ToastsManager
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'detail-conversationtype',
  templateUrl: './conversation-type.detail.component.html'
})
export class ConversationTypeDetailComponent implements OnInit {
  private Id: number = 0;
  public model: any = {
    Id: 0,
    Name: ''
  }
 
  constructor(private router: Router, private conversationType: ConversationTypeService, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr); 
    this.route.params.subscribe(params => {
        this.Id = +params['id']; // (+) converts string 'id' to a number
 
        // In a real app: dispatch action to load the details here.
     });
   }

  ngOnInit() {
    if (this.Id != 0) {
        this.GetObjectById();
    }
    
  }

  GetObjectById(): void {
    this.conversationType.getConversationTypeById(this.Id).then(result => {
        if(result != null && result.success){
            this.model = result.data;
        }
        else {
            this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
        }
    }).catch(function(error) {
        console.log(error);
    })
  }
  Save(type): void {
    if(this.Id == 0) {
        this.conversationType.createConversationType(this.model).then(result => {
            if (result != null && result.success) {
                this.router.navigate(["/conversationtype"]);
                this.toastr.success("Thêm loại hội thoại thành công!!!", "Thông báo");
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
    else {
        this.conversationType.updateConversationType(this.model).then(result => {
            if (result != null && result.success) {
                if (type == 0) {
                    this.router.navigate(["/conversationtype"]);
                    this.toastr.success("Cập nhật loại hội thoại thành công!!!", "Thông báo");
                }
                else {                    
                    this.toastr.success("Cập nhật loại hội thoại thành công!!!", "Thông báo");
                }
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
  }
}
