import { Router } from '@angular/router';
import { AuthenticateService } from '../../services/authenticate.service';
import { OnInit } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { SocketService } from '../../services/socket.service';
import { SessionData } from '../../model/session.data.model';

@Component({ selector: 'logout', templateUrl: 'logout.component.html' })

export class LogoutComponent implements OnInit {
    public socket: any;
    public session: SessionData;
    constructor(private _authenticateService: AuthenticateService, private router: Router,
        private socketService: SocketService) { }
    ngOnInit(): void {
        this._authenticateService.getSessionData().subscribe(res => {
            this.session = res;
        });
        this.socketService.socket.subscribe(o => {
            this.socket = o;
        });
        this._authenticateService.clearSessionData();
        
        this.router.navigate(["/login"]);
    }
}