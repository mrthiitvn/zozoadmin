import { Component, ViewChild, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { UserService } from '../../services/user.service';
import swal from 'sweetalert2';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public title: string = "Cao Hoang Chuyen";
  public userList: any;
  public user: any = {
    userId: null,
    firstName: '',
    lastName: '',
    email: '',
    phone: ''
  }
  public selectedUser: any;
  public valueSearch: any = '';
  public valueRole: any = 1;
  public valueStatus: any = 1;
  public valueSortType: any = 'createdAt DESC';
  public currentPage: number = 1;
  public totalItem: number;
  public pageNum: number = 0;
  public pageSize: number = 10;
  @ViewChild('deleteUserModal') public deleteUserModal: ModalDirective;
  constructor(private userService: UserService,
    private titleService: Title) { 
    this.titleService.setTitle("User");
  }

  ngOnInit() {
    this.GetAll();
  }

  GetAll(): void {
    this.userService.GetAll(this.currentPage, this.valueSortType, this.GetValueSearch(), this.pageSize).then(result => {
      if(result != null && result.success && result.data != null){
        this.userList = result.data.Result;
        this.totalItem = result.data.Result.length;
      }
      else{
        swal('','get all user error', 'error');
      }
    }).catch(function(error){
      swal('',error, 'error');
    });
  }

  GetValueSearch() {
    let searchSend = {
        keySearch: this.valueSearch,
        status: this.valueStatus,
        role: this.valueRole
    };
    return searchSend;
}

  OpenDeleteModal(): void {
    
    this.deleteUserModal.show();
  }

  deleteUser(): void {debugger;
    this.userService.RemoveUser(this.selectedUser).then(result => {
      if(result != null && result.success){
        swal('', result.message, 'success');
      }
      else{
        swal('', result.message, 'error');
      }
    }).catch(function(error){
      swal('', error, 'error');
    })
  }

}
