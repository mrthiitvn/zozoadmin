import { Component, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { PhotoTypeService } from '../../services/photo.type.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
//Import ToastsManager
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'detail-phototype',
  templateUrl: './photo-type.detail.component.html'
})
export class PhotoTypeDetailComponent implements OnInit {
  private Id: number = 0;
  public model: any = {
    Id: 0,
    Name: '',
    ShortName: '',
    Width: null,
    Height: null
  }
 
  constructor(private router: Router, private photoType: PhotoTypeService, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr); 
    this.route.params.subscribe(params => {
        this.Id = +params['id']; // (+) converts string 'id' to a number
 
        // In a real app: dispatch action to load the details here.
     });
   }

  ngOnInit() {
    if (this.Id != 0) {
        this.GetObjectById();
    }
    
  }

  GetObjectById(): void {
    this.photoType.getPhotoTypeById(this.Id).then(result => {
        if(result != null && result.success){
            this.model = result.data;
        }
        else {
            this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
        }
    }).catch(function(error) {
        console.log(error);
    })
  }
  Save(type): void {
    if(this.Id == 0) {
        this.photoType.createPhotoType(this.model).then(result => {
            if (result != null && result.success) {
                this.router.navigate(["/phototype"]);
                this.toastr.success("Thêm loại tệp ảnh thành công!!!", "Thông báo");
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
    else {
        this.photoType.updatePhotoType(this.model).then(result => {
            if (result != null && result.success) {
                if (type == 0) {
                    this.router.navigate(["/phototype"]);
                    this.toastr.success("Cập nhật loại tệp ảnh thành công!!!", "Thông báo");
                }
                else {                    
                    this.toastr.success("Cập nhật loại tệp ảnh thành công!!!", "Thông báo");
                }
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
  }
}
